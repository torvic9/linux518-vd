## vd kernel 5.18

- better support for clang
- support for module signing
- ProjectC (A. Chen) once released
- multigenerational LRU
- block device LED trigger
- cpupower with amd_pstate support
- some btrfs patches from -next
- various fixes, optimisations and backports

See PKGBUILD for source and more details.
